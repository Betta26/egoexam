angular.
  module('projectsApp').
  config(['$routeProvider','$locationProvider',
    function config($routeProvider,$locationProvider) {
      
      $locationProvider.hashPrefix('');

      $routeProvider.
        when('/newProject', {
          template: '<new-project></new-project>'
        }).
        when('/editProject:projectId', {
          template: '<edit-project></edit-project>'
        }).
        when('/detail:projectId', {
          template: '<project-detail></project-detail>'
        }).
        when('/planning:projectId', {
          template: '<project-planning></project-planning>'
        }).
        otherwise('/');
    }
  ]);