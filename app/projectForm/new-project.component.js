angular.
  module('projectsApp').
  component('newProject', {
    templateUrl: 'app/projectForm/form-project.template.html',
    controller: ['projectServiceMock',
      function newProjectController(projectService) {

        this.formName = "New Project";

        this.addProject = function(){

          var newProject = {
            name: this.project.name, 
            description: this.project.description, 
            status: this.project.status, 
            rfi: this.project.rfi, 
            notes: this.project.notes
          };
          projectService.addProject(newProject);
        }

      }
    ]
  });