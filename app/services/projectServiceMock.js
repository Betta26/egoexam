angular.
  module('projectsApp').
    service('projectServiceMock', function() {

    var projects = [
        {
            id: 1,
            name: "Project 1", 
            description: "Description Project 1", 
            status: "Pending", 
            rfi: "RFI Project 1", 
            notes: "Notes Project 1", 
            planning:[
                {date: "01/01/2017", todoTasks: 4, finishedTasks:6},
                {date: "03/01/2017", todoTasks: 8, finishedTasks:2},
                {date: "05/01/2017", todoTasks: 2, finishedTasks:4},
                {date: "06/01/2017", todoTasks: 3, finishedTasks:5}
            ]
        },
        {
            id: 2, 
            name: "Project 2", 
            description: "Description Project 2", 
            status: "Pending", 
            rfi: "RFI Project 2", 
            notes: "Notes Project 2", 
            planning:[
                {date: "01/01/2017", todoTasks: 5, finishedTasks:4},
                {date: "03/01/2017", todoTasks: 8, finishedTasks:2},
                {date: "05/01/2017", todoTasks: 5, finishedTasks:7},
                {date: "06/01/2017", todoTasks: 7, finishedTasks:5}
            ]
        },
        {
            id: 3, 
            name: "Project 3", 
            description: "Description Project 3", 
            status: "Pending", 
            rfi: "RFI Project 3", 
            notes: "Notes Project 3", 
            planning:[
                {date: "01/01/2017", todoTasks: 2, finishedTasks:7},
                {date: "03/01/2017", todoTasks: 9, finishedTasks:5},
                {date: "05/01/2017", todoTasks: 1, finishedTasks:3},
                {date: "06/01/2017", todoTasks: 5, finishedTasks:5}
            ]
        }
    ];

    this.getProjects = function (callback) {
        callback(projects);
    }

    this.addProject = function (project, callback) {
        project.id = projects.length + 1;
        projects.push(project);
        if(callback) callback();
    }

    this.getProject = function (projectId, callback) {
        var project = _.find(projects, function(p) { return p.id == projectId; });
        callback(project);
    }

    this.editProject = function (project, callback) {
        var index = _.findIndex(projects, function(p) { return p.id == project.id; });
        projects[index] = project;
        if(callback) callback();
    }

    this.getProjectPlanning = function (projectId, callback) {
        this.getProject(projectId, function(project) {
            callback(project.planning);
        });
    }

});