angular.
  module('projectsApp').
  component('editProject', {
    templateUrl: 'app/projectForm/form-project.template.html',
    controller: ['$routeParams','projectServiceMock',
      function editProjectController($routeParams,projectService) {
        var self = this;
        this.formName = "Edit Project";
        
        projectService.getProject($routeParams.projectId, function(project) {
          self.project = project;
       
          self.editProject = function(){
            var editedProject = {
            	id: project.id, 
            	name: project.name, 
            	description: project.description, 
            	status: project.status, 
            	rfi: project.rfi, 
            	notes: project.notes
            };
            projectService.editProject(editedProject);
          }
        });
  
      }
    ]
  });