angular.
module('projectsApp').
component('projectPlanning', {
    templateUrl: 'app/projectPlanning/project-planning.template.html',
    controller: ['projectServiceMock','$routeParams',
        function ProjectListController(projectService,$routeParams) {
         	var self = this;

        	projectService.getProjectPlanning($routeParams.projectId, function(projectPlanning) {
				var data = [];
				projectPlanning.forEach(function(planning) {
					data.push({
					   	dates: planning.date,
					    todoTasks: planning.todoTasks,
					    finishedTasks: planning.finishedTasks
					});
				});

				self.data = new wijmo.collections.CollectionView(data);
        	});
        }
    ]
});