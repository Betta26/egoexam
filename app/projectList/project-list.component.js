angular.
module('projectsApp').
component('projectList', {
    templateUrl: 'app/projectList/project-list.template.html',
    controller: ['projectServiceMock',
        function ProjectListController(projectService) {
        	var self = this;
	        projectService.getProjects(function(projects) {
	        	self.projects = projects;
	        });
        }
    ]
});