angular.
  module('projectsApp').
  	service('projectService', function($http) {

    this.getProjects = function (callback) {
      $http.get('/projects').then(function(response) {
          callback(JSON.parse(response.data));
      });
    }

    this.addProject = function (project, callback) {
      $http.post('/project', JSON.stringify(project)).then(function() {
          callback();
      });
    }

    this.getProject = function (projectId, callback) {
      $http.get('/projects/'+projectId).then(function(response) {
          callback(JSON.parse(response.data));
      });
    }

    this.editProject = function (project, callback) {
      $http.put('/project', JSON.stringify(project)).then(function() {
          callback();
      });
    }

    this.getProjectPlanning = function (projectId, callback) {
      $http.get('/planning/'+projectId).then(function(response) {
          callback(JSON.parse(response.data));
      });
    }

});