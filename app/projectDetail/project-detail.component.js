angular.
  module('projectsApp').
  component('projectDetail', {
    templateUrl: 'app/projectDetail/project-detail.template.html',
    controller: ['$routeParams','projectServiceMock',
      function ProjectDetailController($routeParams,projectService) {
        var self = this;
        projectService.getProject($routeParams.projectId, function(project) {
        	self.project = project;
        });
      }
    ]
  });